#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import json
#
# import requests
#
#
# class huangli:
#
# 	@staticmethod
# 	def requests_huangli():
# 		# 接口的url
# 		url = "http://v.juhe.cn/laohuangli/d"
# 		# 接口头部信息
# 		headers = {
# 			"Accept": "*/*",
# 			"Accept-Encoding": "gzip, deflate, br",
# 			"Content-Type": "application/json"
# 		}
# 		# 接口的参数
# 		params = {
# 			"key": "4820422dbea4fc4c6a1e4f3177ff0006",
# 			"date": "2021-01-18",
# 		}
# 		# 发送接口
# 		r = requests.request("post", url, headers=headers, params=params)
# 		r = r.json()
# 		js = json.dumps(r, indent=4, separators=(',', ': '), ensure_ascii=False)
# 		# 打印返回结果
# 		print(js)
#
#
# if __name__ == '__main__':
# 	huangli.requests_huangli()
a = {'max': 200}
b = {'min': 100, 'max': 250}
c = {'min': 50}

# print(a['min'] + b['min'] + c['min'])  # throws KeyError
print(a.get('min', 5) + b.get('min', 0) + c.get('min', 0))  # 150
