#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time

import uiautomator2 as u2


class ZhiFuB:
	def __init__(self):
		# 使用手机安卓ID连接手机
		self.d = u2.connect('31cf2983')

	def zhi_fu_bao(self):
		# 定位支付宝软件，打开支付宝
		# d(text="支付宝").click()
		# 通过包名定位支付宝软件
		self.d.app_start("com.eg.android.AlipayGphone")
		time.sleep(2)
		# 定位蚂蚁森林，打开蚂蚁森林
		# self.d.click(0.309, 0.559)
		self.d(text="蚂蚁森林").click()
		time.sleep(1)  # 睡眠3S
		count = 0
		while True:
			# 点击好友森林，进行偷取能量的准备
			self.d.click(0.845, 0.66)
			time.sleep(1)
			# 定位确认是否存在 “返回我的森林字段”
			if self.d(text="返回我的森林").exists:
				self.d.app_stop("com.eg.android.AlipayGphone")
				break
			count += 1
			# time.sleep(1)

			# 获取好友能量
			self.energy()

			time.sleep(1)

			# 定位确认是否存在 “返回我的森林字段”
			if self.d(text="返回我的森林").exists:
				self.d.app_stop("com.eg.android.AlipayGphone")
				break
			time.sleep(1)

			# 点击好友森林，进行偷取能量的准备
			self.d.click(0.845, 0.66)

		print(f"能量偷完咯~\n一共偷了{count}个人")

	def energy(self):
		# 获取好友能量
		self.d.click(0.118, 0.349)
		self.d.click(0.226, 0.3)
		self.d.click(0.345, 0.271)
		self.d.click(0.492, 0.252)
		self.d.click(0.647, 0.276)
		self.d.click(0.769, 0.322)
		self.d.click(0.856, 0.352)


if __name__ == '__main__':
	z = ZhiFuB()
	z.zhi_fu_bao()
