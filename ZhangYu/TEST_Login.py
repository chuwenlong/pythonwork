#!/usr/bin/env python
# -*- coding: utf-8 -*-



class Loginfor():

    # 封装
    def __init__(self, driver):
        self.driver = driver

    # 定义登陆函数，将登陆作为公共调用的模块，进行数据传递,因此不需要导入webdriver这个模块
    def login(self, username, password):
        # 输入用户名和密码,点击登录
        self.driver.find_element_by_name("email").clear()
        self.driver.find_element_by_name("email").send_keys(username)
        self.driver.find_element_by_name("password").clear()
        self.driver.find_element_by_name("password").send_keys(password)
        self.driver.find_element_by_xpath(
            '//*[@id="login-main-form"]/div/div[3]/button').submit()

