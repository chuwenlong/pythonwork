#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import time


def log():
	# 打日志
	times = time.localtime()
	year = times.tm_year
	mon = times.tm_mon
	day = times.tm_mday
	os.system(f'adb logcat -v time > E:\log\log_{year}{mon}{day}.txt')


if __name__ == '__main__':
	log()
