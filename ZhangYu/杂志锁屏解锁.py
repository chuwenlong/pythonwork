# #!/usr/bin/env python
# # -*- coding: utf-8 -*-

import logging
import os
import time

import uiautomator2 as u2

from 获取手机设备号 import equipment


def log():
    # 打日志
    times = time.localtime()
    year = times.tm_year
    mon = times.tm_mon
    day = times.tm_mday
    os.system(f'adb logcat -v time > E:\log\log_{year}{mon}{day}.txt')


class Magazine_lock_screen:
    def __init__(self, phones, l):
        # 连接手机
        self.d = u2.connect(f'{phones[l]}')
        # 获取屏幕状态
        logging.basicConfig(level = logging.INFO, format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self.logger = logging.getLogger(__name__)

    def bright_screen(self):
        screen = self.d.info
        if not screen["screenOn"]:  # 监测屏幕状态
            self.logger.info("灭屏状态,已亮屏")  # 确认屏幕状态
            self.d.press("power")  # 亮屏
            # 向上滑动解锁
            os.system('adb shell input swipe 800 500  800 50')

        elif screen["screenOn"]:  # 屏幕状态
            self.logger.info("亮屏状态,灭屏")  # 确认屏幕状态
            self.d.press("power")  # 灭屏


if __name__ == '__main__':
    # 获取设备号
    phone = equipment()
    phone_devices = len(phone)
    for lens in range(phone_devices):

        Magazine_lock_screen = Magazine_lock_screen(phone, lens)
        Magazine_lock_screen.bright_screen()

        n = 0
        while n < 100:
            Magazine_lock_screen.bright_screen()
            n += 1
            time.sleep(3)
