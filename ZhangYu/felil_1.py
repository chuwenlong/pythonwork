#!/usr/bin/env python
# -*- coding: utf-8 -*-


import pandas as pd

fileName = 'C:\\Users\\Administrator\\Desktop\\【海鸥】微动壁纸排期(20210201—0226）.xlsx'
fileName1 = 'C:\\Users\\Administrator\\Desktop\\1.xlsx'
sheetName = '02.01'
# 输入一列的数据
'''
s=pd.Series([1,2,3,np.nan,5,6])
print(s.head())
'''

# 使用xlrd查看表格数据
'''
# 打开文件
workBook = xlrd.open_workbook(fileName)
# print(workBook.read())
# 1.获取sheet的名字
# 1.1 获取所有sheet的名字(list类型)
table = workBook.sheets()[0]
row = table.row_values(0)
col = table.col_values(2)
data = table.cell(1,2).value
print(data)
'''
# nan_excle = open(fileName, 'w', newline='')
# print(nan_excle)

books = pd.read_excel(fileName, skiprows=1, usecols="B:G", index_col=None, sheet_name="02.13", dtype='str')
# books = books.replace({2: 10})
books.iat[0, 0] = "ID"
books['Unnamed: 2'][2:] = 'pre'  # 定义添加一列的数据

# 定义指定坐标的单个数据
for l in range(3):
	books.iat[4, l + 1] = "ID"
print(books)

# s = pd.read_excel(fileName, sheet_name=sheetName, dtype='str')
# s = pd.read_excel(fileName, sheet_name=sheetName, dtype='str')
# # s.dropna(inplace=True)
#
# pd.set_option('display.unicode.ambiguous_as_wide', True)
# pd.set_option('display.unicode.east_asian_width', True)
# pd.set_option('display.width', 180)  # 设置打印宽度(**重要**)
# pd.set_option("display.max_columns", None)      #显示所有列
# # pd.set_option("display.max_rows", None)         #显示所有行
# s['Unnamed: 1'][2:] = 'pre'
#
# print(f'{s}\n')
# display(s['Unnamed: 1'])
# df = pd.DataFrame(s)
# df.to_excel(fileName, index=False, header=True)
# print(l)
# DataFrame是表格型数据结构，包含一组有序的列，每列可以是不同的值类型。
# DataFrame有行索引和列索引，可以看成由Series组成的字典。
# df = pd.DataFrame()
# datas = pd.date_range('2021-01-08', periods=6)
# pd = pd.DataFrame(np.random.randn(6, 4), index=datas, columns=['A', 'B', 'C', 'D'])
# print('输入6行4列的表格：')
# print(pd)
# print('\n')
#
# print('输出第二列：')
# print(pd['B'])
# print('\n')


# 通过字典添加数据
'''
print('通过字典创建DataFrame:')
df_1 = pd.DataFrame({'A':1.0,
                     'B': pd.Timestamp(2019,8,19),
                     'C': pd.Series(1,index=list(range(4)),dtype='float32'),
                     'D': np.array([3]*4,dtype='int32'),
                     'E': pd.Categorical(['test','train','test','train']),
                     'F': 'foo'})
print(df_1)
print('\n')
'''

# 返回每列的数据类型
'''
print('返回每列的数据类型：')
print(df_1.dtypes)
print('\n')
'''

# 返回行的序号
'''
print('返回行的序号：')
print(df_1.index)
print('\n')
'''

# 返回行的序号
'''
print('返回列的序号名字：')
print(df_1.columns)
print('\n')
'''

# 把每个值进行打印出来
'''
print('把每个值进行打印出来：')
print(df_1.values)
print('\n')
'''

# 数字总结
'''
print('数字总结：')
print(df_1.describe())
print('\n')
'''

# 翻转数据
'''
print('翻转数据：')
print(df_1.T)
print('\n')
'''

# 按第一列进行排序
'''
print('按第一列进行排序：')
# axis等于1按列进行排序 如ABCDEFG 然后ascending倒叙进行显示
print(df_1.sort_index(1,ascending=False))
print('\n')
'''

# 按某列的值进行排序
'''
print('按某列的值进行排序：')
print(df_1.sort_values('E'))
print('\n')
'''
