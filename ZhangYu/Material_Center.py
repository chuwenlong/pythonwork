#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest,time,random
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

'''
Created on 2020年12月15日 14:49:37
@author: longchuwen
Project:登录掌屿测试用例
'''

class MyTestCase(unittest.TestCase):

	def setUp(self) :
		# self.assertEqual(True,False)
		# 实现不打开浏览器进行web自动化
		self.option=webdriver.FirefoxOptions()
		self.option.set_headless()
		self.driver = webdriver.Firefox(firefox_options=self.option)  # 调起火狐浏览器

		self.driver.maximize_window()  # 窗口最大化
		self.driver.get('http://ts-centerweb.idatachain.cn/#/admin/pageWuliao')  # 输入掌屿网址
		# 输入掌屿账号，龙楚文（测试工程师权限）
		self.driver.find_element_by_xpath('//*[@id="login-main-form"]/div/div[1]/input').send_keys(
			'longchuwen@zookingsoft.com')
		self.driver.find_element_by_xpath('//*[@id="login-main-form"]/div/div[2]/input[1]').send_keys('644265426+lcw')
		# 点击登录按钮，登录掌屿平台
		self.driver.find_element_by_css_selector(
			'html body div.boxd div.boxd_r form#login-main-form div.userBox div button.movd').click()

		time.sleep(3)  # 睡眠3s

		# 断言是否登录成功

		print("...登录执行成功...")

	# def test_Material(self):
	#
	# 	# 鼠标悬浮资源中心
	# 	# mouse = self.driver.find_element_by_xpath('//*[@id="app"]/div/div[2]/div/ul/li[2]/div')
	# 	mouse = self.driver.find_element(By.XPATH,'//*[@id="app"]/div/div[2]/div/ul/li[2]/div')
	# 	ActionChains(self.driver).move_to_element(mouse).perform()
	# 	time.sleep(0.5)
	# 	# 点击物料资源，进入投放物料
	# 	self.driver.find_element(By.XPATH,'/html/body/div[3]/ul/li[2]/ul/a/li').click()
	# 	time.sleep(3)
	#
	# 	# 随机生成物料类型下标
	# 	nub = random.randint(0,23)
	# 	# print(nub)  # 打印物料类型下标
	# 	mouse1 = self.driver.find_elements(By.CSS_SELECTOR,'.imgs-item-img-bg[data-v-5c66f24c]')[1]
	# 	time.sleep(2)
	# 	ActionChains(self.driver).move_to_element(mouse1).perform()
	# 	# 若随点击页面下的物料类型
	# 	if nub in (12,23):
	# 		# 滚动条拉到底部
	# 		js = "var q=document.documentElement.scrollTop = 10000"
	# 		self.driver.execute_script(js)
	# 	time.sleep(5)
	# 	self.driver.find_elements(By.CSS_SELECTOR,'.imgs-item-img-bg[data-v-5c66f24c]')[nub].click()
	# 	time.sleep(5)
	#
	# 	success = 0     # 获取执行总次数
	# 	fail = 0        # 获取执行失败次数
	#
	# 	# 执行关键词搜索50次
	# 	while success < 50:
	# 		try:
	# 			name=[]
	# 			loop=random.randint(1,10)
	# 			for s in range(loop):
	# 				val=random.randint(0x4e00,0x9fbf)
	# 				chr(val)
	# 				name.append(chr(val))
	# 			# print(name)
	# 			# 关键词搜索校验
	# 			self.driver.find_element(By.XPATH,'/html/body/div[1]/div/div[3]/div/div/div[2]/div[2]/div[1]/div[1]/div/input').send_keys(name)
	# 			time.sleep(3)
	# 			# 点击查询按钮
	# 			self.driver.find_element(By.XPATH,'//*[@id="app"]/div/div[3]/div/div/div[2]/div[2]/div[2]/button[1]').click()
	# 			success += 1
	# 			time.sleep(3)
	# 			# 点击重置按钮
	# 			self.driver.find_element(By.XPATH,'//*[@id="app"]/div/div[3]/div/div/div[2]/div[2]/div[2]/button[2]').click()
	# 		except:
	# 			# 当执行失败时，睡眠3秒
	# 			time.sleep(3)
	# 			fail += 1
	# 	print(f'关键词搜索成功执行：{success - fail}次')
	# 	print(f'关键词搜索执行失败：{fail}次')
	# 	time.sleep(3)
	# 	# 点击更多筛选
	# 	self.driver.find_element(By.CSS_SELECTOR,'html body div#app div div.xqList div.home div.page-content div.img-content.content-img-box div.search-box.flex-row-jcsb div.flex-row-jcsb.search-box-right span.more').click()
	# 	time.sleep(2)
	#
	#
	# 	numner = 0      # 获取执行总次数
	# 	numne = 0       # 获取执行失败次数
	#
	# 	# 点击标签查询50次
	# 	while numner < 50:
	# 		numner += 1
	# 		# 获取标签下标进行查询
	# 		attribute=random.randint(0,72)
	# 		# print(attribute)
	# 		time.sleep(3)
	#
	# 		try:
	# 			# 点击人物属性及其属性标签查询
	# 			if 35 >= attribute >= 28:
	# 				self.driver.find_elements(By.CSS_SELECTOR,'.el-tabs__item')[2].click()
	# 				self.driver.find_elements(By.CSS_SELECTOR,'.label-box-item[data-v-2b2f8c90]')[attribute].click()
	# 			# 点击动物属性及其属性标签查询
	# 			elif 41 >= attribute >= 36:
	# 				self.driver.find_elements(By.CSS_SELECTOR,'.el-tabs__item')[3].click()
	# 				self.driver.find_elements(By.CSS_SELECTOR,'.label-box-item[data-v-2b2f8c90]')[attribute].click()
	# 			# 点击汽车属性及其属性标签查询
	# 			elif 44 >= attribute >= 42:
	# 				self.driver.find_elements(By.CSS_SELECTOR,'.el-tabs__item')[4].click()
	# 				self.driver.find_elements(By.CSS_SELECTOR,'.label-box-item[data-v-2b2f8c90]')[attribute].click()
	# 			# 点击风景属性及其属性标签查询
	# 			elif 49 >= attribute >= 45:
	# 				self.driver.find_elements(By.CSS_SELECTOR,'.el-tabs__item')[5].click()
	# 				self.driver.find_elements(By.CSS_SELECTOR,'.label-box-item[data-v-2b2f8c90]')[attribute].click()
	# 			# 点击家属属性及其属性标签查询
	# 			elif 53 >= attribute >= 50:
	# 				self.driver.find_elements(By.CSS_SELECTOR,'.el-tabs__item')[6].click()
	# 				self.driver.find_elements(By.CSS_SELECTOR,'.label-box-item[data-v-2b2f8c90]')[attribute].click()
	# 			# 点击植物属性及其属性标签查询
	# 			elif 55 >= attribute >= 54:
	# 				self.driver.find_elements(By.CSS_SELECTOR,'.el-tabs__item')[7].click()
	# 				self.driver.find_elements(By.CSS_SELECTOR,'.label-box-item[data-v-2b2f8c90]')[attribute].click()
	# 			# 点击生活属性及其属性标签查询
	# 			elif 66 >= attribute >= 56:
	# 				self.driver.find_elements(By.CSS_SELECTOR,'.el-tabs__item')[8].click()
	# 				self.driver.find_elements(By.CSS_SELECTOR,'.label-box-item[data-v-2b2f8c90]')[attribute].click()
	# 			# 点击颜色属性及其属性标签查询
	# 			elif 72 >= attribute >= 67:
	# 				self.driver.find_elements(By.CSS_SELECTOR,'.el-tabs__item')[9].click()
	# 				self.driver.find_elements(By.CSS_SELECTOR,'.label-box-item[data-v-2b2f8c90]')[attribute].click()
	# 			# 点击属性以外的标签
	# 			else:
	# 				self.driver.find_elements(By.CSS_SELECTOR,'.label-box-item[data-v-2b2f8c90]')[attribute].click()
	# 		except:
	# 			# 执行失败睡眠3秒
	# 			time.sleep(3)
	# 			numne += 1
	# 			print(f'壁纸标签查询报错下标：{attribute}')
	#
	# 	print(f'壁纸标签查询成功执行：{numner-numne}次')
	# 	print(f'壁纸标签查询执行失败：{numne}次')
	# 	time.sleep(5)
	#
	# 	try:
	# 		self.driver.refresh()
	# 	except Exception as e:
	# 		print('END')
	#
	# 	time.sleep(5)


	def test_Newly_build(self):

		# 鼠标悬浮资源中心
		# mouse = self.driver.find_element_by_xpath('//*[@id="app"]/div/div[2]/div/ul/li[2]/div')
		mouse=self.driver.find_element(By.XPATH, '//*[@id="app"]/div/div[2]/div/ul/li[2]/div')
		ActionChains(self.driver).move_to_element(mouse).perform()
		time.sleep(3)
		# 点击物料资源，进入投放物料
		self.driver.find_element(By.XPATH, '/html/body/div[3]/ul/li[2]/ul/a/li').click()
		time.sleep(3)

		error_mes = self.driver.find_element(By.XPATH,'/html/body/div[1]/div/div[3]/div/div/div[2]/div[1]/div[2]/div/span[1]').text
		print(error_mes)
		# 随机生成物料类型下标
		# nub = 0
		# # print(nub)  # 打印物料类型下标
		# mouse1 = self.driver.find_elements(By.CSS_SELECTOR,'.imgs-item-img-bg[data-v-5c66f24c]')[1]
		# time.sleep(2)
		# ActionChains(self.driver).move_to_element(mouse1).perform()
		# # 若随点击页面下的物料类型
		# if nub in (12,23):
		# 	# 滚动条拉到底部
		# 	js="var q=document.documentElement.scrollTop = 10000"
		# 	self.driver.execute_script(js)
		# time.sleep(5)
		# self.driver.find_elements(By.CSS_SELECTOR,'.imgs-item-img-bg[data-v-5c66f24c]')[nub].click()
		# time.sleep(5)

		self.driver.find_element(By.XPATH,'//*[@id="app"]/div/div[3]/div/div/div[2]/div[8]/div[1]/img').click()

		time.sleep(2)



		# # 点击【New Tag】进入标签编辑页面
		# New_Tag = random.randint(0, 1)
		# time.sleep(1)
		# self.driver.find_elements(By.CSS_SELECTOR,'[data-v-49cb142a] .button-new-tag')[New_Tag].click()
		# time.sleep(1)
		#
		# # 查询标签搜索
		# chaxun = self.driver.find_element(By.XPATH,'/html/body/div[1]/div/div[3]/div/div/div[2]/div[3]/div[26]/div/div/section/div[1]/div[1]/div/div[1]/input')
		# chaxun.click()
		# time.sleep(3)
		# chaxun.send_keys('三星')
		# time.sleep(3)
		# chaxun.send_keys(Keys.ENTER)
		# time.sleep(2)
		#
		#
		# shuzi=0
		# while shuzi < 1:
		# 	xiabiao = 0
		# 	xiabiao1 = 0
		# 	try:
		# 		biaoqian=random.randint(92,112)
		# 		# print(biaoqian)
		# 		self.driver.find_elements(By.CSS_SELECTOR, '.el-select-dropdown__item')[biaoqian].click()
		# 		time.sleep(2)
		# 		shuzi += 1
		# 		xiabiao += 1
		# 	except:
		# 		time.sleep(2)
		# 		xiabiao1 += 1
		# self.driver.find_element(By.XPATH,'/html/body/div[1]/div/div[3]/div/div/div[2]/div[3]/div[26]/div/div/section/div[1]/div[1]/div/div[1]/input').send_keys(Keys.ESCAPE)
		# time.sleep(3)
		#
		# print(f'查询壁纸标签查询成功执行：{xiabiao - xiabiao1}次')
		# print(f'查询壁纸标签查询执行失败：{xiabiao1}次')
		#
		# gongneng = self.driver.find_element(By.XPATH,'/html/body/div[1]/div/div[3]/div/div/div[2]/div[3]/div[26]/div/div/section/div[1]/div[2]/div/div/div[2]')
		# gongneng.click()
		# time.sleep(1)
		#
		# s = 0
		# while s < 1:
		# 	n = random.randint(0,3)
		# 	s += 1
		#
		# 	m = 0
		# 	m1 = 0
		# 	try:
		# 		if n == 0:
		# 			self.driver.find_elements(By.CSS_SELECTOR,'.el-cascader-node__label')[n].click()
		# 			time.sleep(1)
		# 			n1 = random.randint(4,6)
		# 			self.driver.find_elements(By.CSS_SELECTOR,'.el-checkbox__input')[n1].click()
		# 		elif n == 1:
		# 			self.driver.find_elements(By.CSS_SELECTOR,'.el-cascader-node__label')[n].click()
		# 			time.sleep(1)
		# 			n1=random.randint(4,24)
		# 			self.driver.find_elements(By.CSS_SELECTOR,'.el-checkbox__input')[n1].click()
		# 		elif n == 2:
		# 			self.driver.find_elements(By.CSS_SELECTOR,'.el-cascader-node__label')[n].click()
		# 			time.sleep(1)
		# 			n1=random.randint(4,14)
		# 			self.driver.find_elements(By.CSS_SELECTOR,'.el-checkbox__input')[n1].click()
		# 		else:
		# 			self.driver.find_elements(By.CSS_SELECTOR,'.el-cascader-node__label')[n].click()
		# 			time.sleep(1)
		# 			n1=random.randint(4,11)
		# 			self.driver.find_elements(By.CSS_SELECTOR,'.el-checkbox__input')[n1].click()
		# 		m+=1
		# 	except:
		# 		time.sleep(3)
		# 		m1 += 1
		#
		# print(f'查询壁纸标签查询成功执行：{m-m1}次')
		# print(f'查询壁纸标签查询执行失败：{m1}次')
		#
		# time.sleep(3)
		# gongneng.click()
		# time.sleep(3)
		#
		# try:
		# 	rad = random.randint(0,1)
		# 	if rad == 0:
		# 		self.driver.find_element(By.XPATH,'/html/body/div[1]/div/div[3]/div/div/div[2]/div[3]/div[26]/div/div/section/div[2]/button[1]').click()
		# 	else:
		# 		self.driver.find_element(By.XPATH,'/html/body/div[1]/div/div[3]/div/div/div[2]/div[3]/div[26]/div/div/section/div[2]/button[2]').click()
		# 	time.sleep(3)
		# except:
		# 	time.sleep(3)

		# try:
		# 	self.driver.refresh()
		# except Exception as e:
		# 	print('END')


	# def tearDown(self):
	# 	try:
	# 		self.driver.refresh()
	# 	except Exception as e:
	# 		print('END')

if __name__ == '__main__':
	unittest.main()

