# #!/usr/bin/env python
# # -*- coding: utf-8 -*-

import logging
import multiprocessing as np
import os
import time

import uiautomator2 as u2

from 获取手机设备号 import equipment

phone = equipment()
phone_devices = len(phone)

print(phone)


def test_case(i):  # 执行用例
    d = u2.connect(phone[int(i)])  # d = u2.connect('192.168.1.117')#  uiautomator2 连接手机
    MultiDevice(d, i)


def MultiDevice(d, i):  # 功能执行
    logging.basicConfig(level = logging.INFO, format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    logger = logging.getLogger(__name__)

    screen = d.info

    n = 0
    while n < 10000:
        if not screen["screenOn"]:  # 监测屏幕状态
            logger.info(f"手机 {phone[int(i)]} 灭屏状态,已亮屏")  # 确认屏幕状态
            d.press("power")  # 亮屏
            time.sleep(3)
            # 向上滑动解锁
            os.system(f"adb -s {phone[int(i)]} shell input keyevent 82")
            n += 1
            time.sleep(3)

        elif screen["screenOn"]:  # 屏幕状态
            logger.info(f"手机 {phone[int(i)]} 亮屏状态,灭屏")  # 确认屏幕状态
            d.press("power")  # 灭屏
            n += 1
            time.sleep(1)


def main():  # 多进程

    for i in range(len(phone)):  # 有几个设备起几个进程
        p = np.Process(target = test_case, args = (str(i)))
        p.start()
        time.sleep(3)


if __name__ == '__main__':
    main()
