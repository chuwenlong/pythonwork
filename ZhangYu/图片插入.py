#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import os
from PIL import Image
import requests as re
import pandas as pd
import numpy as np
from IPython.display import display


# ext = ['jpg', 'jpeg', 'png', 'webp']
# flies = os.listdir('.')
# '''
# 批量将图片压缩成统一的大小：270*660
# 将图片插入EXCEL表格
# 批量将图片插入excel表格
# '''
# filename = 'C:/Users/Administrator/Desktop/image/音乐/note.png'
# t = Image.open(filename)
# print(t)

def requstes_1():
	# 获取鉴权码token值
	token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI0IiwianRpIjoiNDhhMDc4Mzc1OTYyNzAxY2I2ZWM3ZDI3M2UwOWNiMjQyZDE4NDRjNTA1MmU3NjNkZDk1MmVhODgwZjNhNzgzNTNjYmVmNGZmN2Q5M2NiMWMiLCJpYXQiOjE2MTE4MzczNzYsIm5iZiI6MTYxMTgzNzM3NiwiZXhwIjoxNjQzMzczMzc2LCJzdWIiOiIyMjkiLCJzY29wZXMiOltdfQ.oSDGpFIUlJRH3nSMi4Va-IFn1l12Ww7aJAq3OH_2r0UOeYNgAgnREy21uCX7b1iS9sDerdYfg9yOetamXD2r8G2vzfzg2iuP_7i0pqTea7_54QzU0ikQRzXgO6VLrZw-sSEbth9X09wyPxudZk9Je5A_nW0SyEgWi2JsehzyEdrVMCCoQz-5N8QEPfOO7D2oC_3wcUGUuOsQLMz0wrDm0UInXOZIdSh_NIPeP-Q7BOJLoe0CN-rHJOCXGRukNiRLIlJJIqs9XxctEaObutMsRAWjfcv5Uh7BjqAYu1iIC0xl-0nlW-Dy4mdSlaYlsSdHylsgkqh3e0LZnkR6QoMLnJ6quKCv4UU11iW8rrM93tcRwCvjepmAbcz14bScS5jz0ETZ3-al6NUd45QzxESbnFbIzjO06mTqsLWe__WknvRs2SmYZFUP0cVzYYq9pFYqxaEdiZcwOSpinpz1eEKK_7ybIL4rcualEUFAQn5EK3lBCg2slK1m8j3RQTfVrgkeR8ETfcuv3lmbBuMpCH3MP75-r6KWFElflKoCMdxQ4R1oyMOWvS8evDMnMv_MoYNyWbiWwq97A8IXGzKCs71rZHy3pGQ5INC9Em9-3uzxxHUomiD4tbfImUmDypPkj97QSRzeFF76NpUzpY0xk8NLRXANjMJxqzjPlG6iutv1J1c"

	'''
	通过URL，请求体，请求头部，获取接口信息
	'''
	url = 'https://c2.zookingsoft.com/api/pushlib/common/source/search'
	params = {
		"p": "10",
		"page": "1",
		"tdate": "2021-01-29",
		"plid": "PD_19",
		"audit_type": "13"
	}
	headers = {
		'Content-Type': 'application/json;charset=UTF-8',
		'Parkingwang-Client-Source': 'ParkingWangAPIClientWeb',
		'authorization': f'{token}'
	}

	r = re.get(url, params=params, headers=headers)
	r = r.json()['data']["data"]    # 获取对应的接口数据
	return r

def data_1():
	# 将返回数据提取到列表中
	r = requstes_1()
	a = []

	'''
	mfid1           物料ID
	wpid1           定投md5
	wpid_tags_id1   K标签
	'''
	mfid1 = []
	wpid1 = []
	wpid_tags_id1 = []

	for l in r:
		a.append(l)

	for s in range(len(a)):
		mfid1.append(a[s]['mfid'])
		w = a[s]['mfinal']['wpid']
		wpid1.append(w)
		wt_id = a[s]['mfinal']['wpid_tags_id']
		wpid_tags_id1.append(wt_id)
	# print(mfid1)
	# print(wpid1)
	# print(wpid_tags_id1)

	# js = json.dumps(a, indent=4, ensure_ascii=False, separators=(',', ':'))
	# print(js)

	# for l in range(len(mfid1)):
	# 	s = mfid1[l]
	# 	image = pd.DataFrame(s, index=None)
	# # books = pd.Series([s])
	# print(image)

	# 将数据填写入excel表格
	df = pd.DataFrame({
		'图片': '     ',
		'物料ID': mfid1,
		'壁纸标识': wpid1,
		'K标签': wpid_tags_id1
	}, dtype=str)
	df.reset_index().rename(columns={'物料ID': mfid1})
	# 修改索引
	# df = df.set_index(['图片'])
	# df.to_excel('C:/Users/Administrator/Desktop/image/音乐/NamesAndAges.xlsx', sheet_name='2021-01-29')
	display(df)
	# print(df)
	return mfid1, wpid1, wpid_tags_id1

# def felil_1():
#
# 	books = pd.Series([1, 2, 3, np.nan, 5, 6])
# 	print(books)


if __name__ == "__main__":
	# requstes_1()
	data_1()
	# felil_1()
