import json
import unittest,time

import requests
from selenium import webdriver
from selenium.webdriver.common.by import By


class MyTestCase(unittest.TestCase):

	def setUp(self) -> None:

		self.option = webdriver.FirefoxOptions()
		# self.option.set_headless()
		self.driver = webdriver.Firefox(firefox_options=self.option)
		self.driver.get("http://zookingsoft.gitee.io/annual-meeting-pc/")

		time.sleep(3)
		# self.driver.find_element_by_xpath('/html/body/div/div[1]').click()
		# time.sleep(1)

	def test_something(self):
		# self.assertEqual(True,False)


		CISHU = 0
		# 抽取特等奖计数
		Grand = 0
		Prize = 0
		# 抽取一等奖计数
		the_first = 0
		the_prize = 0
		# 抽取二等奖计数
		second = 0
		award = 0
		# 抽取三等奖计数
		third = 0
		the_award = 0
		# 抽取神秘奖计数
		del_data = 0
		Mystery = 0
		Award = 0

		while CISHU < 1:

			# try:
			# 	self.driver.find_element(By.XPATH,'//*[@id="luck-draw-btn"]').click()
			# 	time.sleep(1)
			# except Exception as e:
			# 	print(f'进入抽奖页面报错，报错信息是：{e}')

			# 抽取特等奖
			try:
				self.driver.find_element(By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/div/div[10]/div/div').click()
				time.sleep(1)
				self.driver.find_element(By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/div/div[10]/div/img').click()
				time.sleep(2)
				self.driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div').click()
				time.sleep(1)
				self.driver.find_element(By.XPATH, '/html/body/div/div[3]/div[4]/div').click()
				time.sleep(1)
				self.driver.find_element(By.XPATH, '/html/body/div/div[3]/div[1]/div').click()
				time.sleep(1)
				Grand += 1
			except Exception as e:
				print(f'抽取特等奖页面报错，报错信息是：{e}')
				Prize += 1

			# 抽取一等奖
			try:
				self.driver.find_element(By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/div/div[10]/div/ul/li[1]/img').click()
				time.sleep(1)
				self.driver.find_element(By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/div/div[10]/div/img').click()
				time.sleep(2)
				self.driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div').click()
				time.sleep(1)
				self.driver.find_element(By.XPATH, '/html/body/div/div[3]/div[4]/div').click()
				time.sleep(1)
				self.driver.find_element(By.XPATH, '/html/body/div/div[3]/div[1]/div').click()
				time.sleep(1)
				the_first += 1
			except Exception as e:
				print(f'抽取一等奖页面报错，报错信息是：{e}')
				the_prize += 1

			# 抽取二等奖
			try:
				self.driver.find_element(By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/div/div[10]/div/ul/li[2]/img').click()
				time.sleep(1)
				self.driver.find_element(By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/div/div[10]/div/img').click()
				time.sleep(2)
				self.driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div').click()
				time.sleep(1)
				self.driver.find_element(By.XPATH, '/html/body/div/div[3]/div[4]/div').click()
				time.sleep(1)
				self.driver.find_element(By.XPATH, '/html/body/div/div[3]/div[1]/div').click()
				time.sleep(1)
				second += 1
			except Exception as e:
				print(f'抽取二等奖页面报错，报错信息是：{e}')
				award += 1

			# 抽取三等奖
			try:
				self.driver.find_element(By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/div/div[10]/div/ul/li[3]/img').click()
				time.sleep(1)
				self.driver.find_element(By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/div/div[10]/div/img').click()
				time.sleep(2)
				self.driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div').click()
				time.sleep(1)
				self.driver.find_element(By.XPATH, '/html/body/div/div[3]/div[4]/div').click()
				time.sleep(1)
				self.driver.find_element(By.XPATH, '/html/body/div/div[3]/div[1]/div').click()
				time.sleep(1)
				third += 1
			except Exception as e:
				print(f'抽取三等奖页面报错，报错信息是：{e}')
				the_award += 1

			del_data = 0
			while del_data < 166:
				# 抽取神秘抽奖
				try:
					self.driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div').click()
					time.sleep(1)
					self.driver.find_element(By.XPATH, '/html/body/div/div[3]/div[4]/div').click()
					time.sleep(1)
					self.driver.find_element(By.XPATH, '/html/body/div/div[3]/div[1]/div').click()
					time.sleep(1)
					Mystery += 1
				except Exception as e:
					print(f'抽取神秘抽奖页面报错，报错信息是：{e}')
					# self.driver.find_element(By.XPATH, '/html/body/div/div[3]/div[1]/div').click()
					Award += 1
				del_data += 1


			# 删除抽奖数据
			uri = {
					'0': "http://a.zookingsoft.com/cors/init_vote_item",  # 初始化投票结果
					'1': "http://a.zookingsoft.com/cors/del_vote_data",  # 删除投票数据
					'2': "http://a.zookingsoft.com/cors/init_draw_data"  # 初始化抽奖结果
					# '3': "http://ts-i.idatachain.cn/api/material/tags/search/all?type=UC&material=2"
			}

			for l in range(3):
				url = uri[f'{l}']
				r = requests.request("GET", url)
				d = json.loads(r.text)
				js = json.dumps(d, sort_keys=True, indent=4, separators=(',', ': '), ensure_ascii=False)
				print('删除抽奖数据成功')

			CISHU += 1
			print(f'抽取特等奖成功 {Grand} 次')
			print(f'抽取特等奖失败 {Prize} 次')

			print(f'抽取一等奖成功 {the_first} 次')
			print(f'抽取一等奖失败 {the_prize} 次')

			print(f'抽取二等奖成功 {second} 次')
			print(f'抽取二等奖失败 {award} 次')

			print(f'抽取三等奖成功 {third} 次')
			print(f'抽取三等奖失败 {the_award} 次')

			print(f'抽取神秘奖成功 {Mystery} 次')
			print(f'抽取神秘奖失败 {Award} 次')


	def test_something2(self):

		a = 0
		b = 0
		try:
			for l in range(1,10):
				self.test_something()
				a += 1
		except Exception as e:
			print(f'抽空总流程报错：{e}')
			b += 1
		print(f'抽空总流程执行次数：{l}')
		print(f'抽奖总流程支持成功次数：{a}')
		print(f"抽奖总流程执行失败：{b}")

	# def tearDown(self):
	# 	self.driver.quit()

if __name__=='__main__':
	unittest.main()
