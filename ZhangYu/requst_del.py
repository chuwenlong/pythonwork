#!/usr/bin/env python
# -*- coding: utf-8 -*-


import json

import requests as re

# url = "https://c2.zookingsoft.com/api/pushlib/textlink/del"
# 内置资源库
url = "http://ts-i.idatachain.cn/api/pushlib/common/source/del"
i = 13600
time = "2021-03-05"
while True:
	if i <= 13649:
		i += 1

		l = [{"plid": "PD_5-S_6", "mfid": f"NSLS_{i}", "tdate": time}]
		l = json.dumps(l)
		# print(l)

		headr={
			"accept": "application/json, text/plain, */*",
			"authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI0IiwianRpIjoiOWM3NTlkYmFjYjc4YzNmNmNlMzdjZGUzMWE0ZjRhZTMyNDRkMWMyZTBhZWZiYzk3YTU0ODdmOGQ4NjIxZWMyZGY4N2VkYzdmNmExZTFhZTIiLCJpYXQiOjE2MTQzMzE4ODgsIm5iZiI6MTYxNDMzMTg4OCwiZXhwIjoxNjQ1ODY3ODg4LCJzdWIiOiIyMjkiLCJzY29wZXMiOltdfQ.ZuzZsF0TQnLJSZZp8o08NxB8adsVCVRJO6BRbCSF6K3JQRRem-of9rTbu40u2aMs4y6cJ7zr_JOvFvEseRs2D0NTATonN2M92l9uZPntWjnKRFIXXDRwxlOd1AwWExGHfSXNVcYC_TdeltIoy5O66sxms5XSJP5_g0pgPviRiRdJALkjn0coK-SQXexKlIdfnV97-yd65fenqNYuMkGmiMXvkYfp2_vzY6I_t-tuDuf_gJxdlVVT6RLoXjiSJG8NG2Dmo6932wNxtpxibjznwgz89EMVWCpDw4GxMztoVqgyFE-UGjDHUvkmMMGw3p5uIJx6j62g4hnmxgZl8VlR2pTWLdS7jTROOuKqAtv-HlpPXVrLLXlxiqfA41u64vfliiWrfaqO3pWS45-rPC1VH_e1wQmspF95gFjhvYdyTb69KoBqUmbtIDUwS4t0yscq4pHaOfMEA11Rq5L5e5YxlFE-JXln9MdHSI4RikjlK5_5ECrtjUG1Zu6wc3WwmRFjD8YsaA4gzejuFpX8BEPguctfKK4VlpZWr2z4-kJrUsyHXs0NIJqv6Pn4yknw7pyTAnGWRE9h5Z1Z0sAKX43t8ssDoHnr73-bk56cPXSkuq5bcw7qrGtZO8cJzPr3DDFYpOd2RSw-LFkixgQV_TpQo6Qec6t95FzgCbBOQ0Jqp_g"
		}

		parms={
			"textlink": l,
			"plid": "PD_5",
			"sub_plid": "PD_5-S_6"
		}

		r = re.post(url,data=parms,headers=headr)
		d = json.loads(r.text)
		js = json.dumps(d,sort_keys=True,indent=4,separators=(',',': '),ensure_ascii=False)
		print(js)

	else:
		break




