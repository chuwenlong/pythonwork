#!/usr/bin/env python
# -*- coding: utf-8 -*-
# https://movie.douban.com/j/subject_abstract?subject_id=35096844\

import json

import requests as re


# 电影详情
def movie_details():
	url = "https://movie.douban.com/j/subject_abstract?"

	params = {
		"subject_id": "35096844"
	}

	headers = {
		"Authorization": "token",
		"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36 Edg/89.0.774.54"
	}

	movies = re.get(url, params=params, headers=headers)
	js = movies.json()
	js = js["subject"]
	js = js["actors"]
	# 主演：actors， 导演：directors，时长：duration，电影：id
	data = json.dumps(js, indent=0, separators=(',', ': '), ensure_ascii=False)  # 字符串转化
	print(data)


if __name__ == '__main__':
	movie_details()
