#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import time

'''
配置全局参数，
'''

project_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# 浏览器驱动存放路径
Firefox_driver_path = project_path + '\\driver\\geckodriver.exe'
time.sleep(3)

if __name__ == '__main__':
	test1 = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)[0]), '.'))
