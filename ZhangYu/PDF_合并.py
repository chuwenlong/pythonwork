#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

# from PyPDF2 import PdfFileMerger
#
# fileName = ['E:/Personal reimbursement/1月/1月7日/电子发票.pdf', 'E:/Personal reimbursement/1月/1月7日/高德打车电子行程单.pdf']
# file_merger = PdfFileMerger()
# for pdf in fileName:
#     file_merger.append(pdf)
# file_merger.write("E:/Personal reimbursement/1月/merge.pdf")

# !usr/bin/env python
# encoding:utf-8

"""
功能：找出来一个字符串中最长不重复子串
"""
# tup = tuple("indivisibility")
# list1 = []
# agg = set()
# for l in tup:
# 	if l in list1:
# 		agg.add(l)
# 	else:
# 		list1.append(l)
#
# print(len(agg))


numbers = "6 7 8 9 10"

# numbers1 = numbers.split()
#
# minNum = numbers1[0]
# maxNum = numbers1[0]
# for item in numbers1:
# 	if int(item) < int(minNum):
# 		minNum = item
# 	elif int(item) > int(maxNum):
# 		maxNum = item
# nn = []
# for s in numbers.split(" "):
# 	nn.append(int(s))
# maxNum = max(s)
# minNum = min(s)

# nn = [int(s) for s in numbers.split(" ")]

# print(max(nn), min(nn))


"""
给字符串进行排序
"""
sentence1 = "Th1is a3 is2 T4est"

# sentence = sentence.split()
# s = []
# count = 0
# while count <= len(sentence):
# 	for item in sentence:
# 		if item.find(f'{count}') >= 0:
# 			s.append(item)
# 	count += 1
# string = " ".join(s)
# print(string)

# def order(words):
#     return ' '.join(sorted(words.split(), key=lambda w: sorted(w)))
#
# print(order(sentence1))

# def reverse_words(text):
#     #go for it
#     return text[::-1]
#
# print(reverse_words('The quick brown fox jumps over the lazy dog.'))

string = 'double  spaced  words'

# str1 = []
# # inversion = [str1.append(l[::-1]) for l in string]
# for l in string.split(" "):
# 	str1.append(l[::-1])
# print(' '.join(str1))

# def reverse_words(str):
# 	return ' '.join(s[::-1] for s in str.split(' '))
#
#
# print(reverse_words(string))

prod = 4895

# n = 0
# a = 0
# b = 1
# while True:
# 	if n == 0 or n == 1:
# 		pass
# 	elif a * b == prod:
# 		break
#
# 	# elif a*b != prod and min(prod-(a*b)):
# 	# 	print(a, b, False)
# 	# 	break
# 	else:
# 		a, b = b, a + b
# 		if a * b == prod:
# 			break
# 	n += 1
# print(a, b, True)

# def productFib(prod):
# 	n = 0
# 	a = 0
# 	b = 1
# 	list1 = []
# 	while True:
# 		if n == 0 or n == 1:
# 			pass
# 		elif a * b == prod:
# 			list1.append(a)
# 			list1.append(b)
# 			list1.append(True)
# 			break
#
# 		elif a*b > prod:
# 			list1.append(a)
# 			list1.append(b)
# 			list1.append(False)
# 			break
#
# 		else:
# 			a, b = b, a + b
# 			if a * b == prod:
# 				list1.append(a)
# 				list1.append(b)
# 				list1.append(True)
# 				break
# 			elif a * b > prod:
# 				list1.append(a)
# 				list1.append(b)
# 				list1.append(False)
# 				break
# 		n += 1
#
# 	return list1

# def productFib(prod):
# 	a, b = 0, 1
# 	while prod > a * b:
# 		a, b = b, a + b
# 	return [a, b, prod == a * b]

#
# print(productFib(4895))
# print(productFib(5895))

# people = [25, 100]
# def tickets(people):
# 	people1 = people[:]
# 	agg = set()
# 	for item in people:
#
# 		if item == 25:
# 			agg.clear()
# 			agg.add("YES")
#
# 		elif item == 50:
# 			if 25 not in people1:
# 				agg.clear()
# 				agg.add("NO")
# 			# break
# 			elif 25 in people1:
# 				people1.remove(25)
# 				agg.clear()
# 				agg.add("YES")
#
# 		elif item == 100:
# 			if 25 not in people1 or 50 not in people1:
# 				agg.clear()
# 				agg.add("NO")
# 			elif 50 not in people1 and people1.count(25) >= 4:
# 				people1.remove(25)
# 				people1.remove(25)
# 				people1.remove(25)
# 				agg.clear()
# 				agg.add("YES")
# 			elif 25 in people1 and 50 in people1:
# 				people1.remove(25)
# 				people1.remove(50)
# 				agg.clear()
# 				agg.add("YES")
# 		else:
# 			agg.add("NO")
# 	agg = "".join(agg)
# 	return agg
#
#
# print(tickets([25, 25, 50]))
# print(tickets([]))

x = random.random()
print(x)
