#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
import random
import re
import time

import uiautomator2 as u2


class Mango:
    def __init__(self):
        self.y = random.uniform(0.110, 0.500)
        self.x = random.uniform(0.110, 0.500)
        os.system('adb version')  # os.system是不支持读取操作的

        # 读取设备 id
        readDeviceId = list(os.popen('adb devices').readlines())
        deviceId = re.findall(r'^\w*\b', readDeviceId[1])[0]

        # 获取安卓ID
        self.out = os.popen('adb shell settings get secure android_id').read()  # os.popen支持读取操作

        # 连接手机
        self.d = u2.connect(f'{deviceId}')

    def open_mango(self):
        n = 0
        try:
            # 打开芒果TV
            self.d.app_start("com.hunantv.imgo.activity")
            # self.d.app_start("com.jsmcc")

            time.sleep(10)
        except Exception as e:
	        self.d.app_stop("com.hunantv.imgo.activity")

            n += 1
        print("执行错误 " + str(n) + " 次")

    def operation_mango(self):
        # 点击指定坐标
        self.d.click(self.x, self.y)

        time.sleep(5)
        n = 0
        try:
	        # 垂直滚动到页面最底部/横向滚动到最右侧
	        self.d(scrollable = True).scroll.toEnd()

	        time.sleep(5)
	        # self.d(resourceId="com.android.systemui:id/back").click()
	        # self.d(scrollable=True).scroll.horiz.toEnd()
	        #
	        # time.sleep(5)
	        # self.d(resourceId="com.android.systemui:id/home").click()
	        #
	        # time.sleep(5)

	        # 垂直滚动到页面顶部/横向滚动到最左侧
	        # self.d(scrollable=True).scroll.toBeginning()
	        #
	        # time.sleep(5)
	        # self.d(scrollable=True).scroll.horiz.toBeginning()

	        time.sleep(5)

        except Exception as e:
	        self.d.app_stop("com.hunantv.imgo.activity")

            n += 1
        print("执行错误 " + str(n) + " 次")

    def back_tv(self):
        m = 0
        try:
            # 点击back键
            self.d(resourceId="com.android.systemui:id/back").click()
            time.sleep(5)
        except Exception as e:
	        self.d.app_stop("com.hunantv.imgo.activity")

            m += 1
        print("执行错误 " + str(m) + " 次")

    def close_mango(self):
        m = 0
        try:
	        # 关闭芒果TV
	        self.d.app_stop("com.hunantv.imgo.activity")
	        # self.d.app_stop("com.jsmcc")

	        self.d(resourceId = "com.android.systemui:id/home").click()
	        time.sleep(8)

        except Exception as e:
	        self.d.app_stop("com.hunantv.imgo.activity")

            m += 1
        print("执行错误 " + str(m) + " 次")

    def tv_go(self):
        n = 0
        while n < 100:
            self.open_mango()
            # self.operation_mango()
            self.back_tv()
            self.close_mango()
            # self.clear_mango()
            n += 1
        n = 0
        while n < 100:
            self.open_mango()
            self.back_tv()
            self.back_tv()
            self.back_tv()
            self.open_mango()
            # self.clear_mango()
            n += 1

    @staticmethod
    def clear_mango():
        # 清除芒果TV数据
        print("芒果TV数据清除成功")
        return os.system('adb shell pm clear com.hunantv.imgo.activity')


if __name__ == '__main__':
    Mango = Mango()
    Mango.tv_go()
