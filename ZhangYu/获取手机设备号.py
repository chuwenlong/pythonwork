#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re


def equipment():
	os.system('adb version')  # os.system是不支持读取操作的

	# 读取设备 id
	readDeviceId = list(os.popen('adb devices').readlines())
	length = len(readDeviceId)
	facility = []
	for l in range(length - 1):
		if l > 0:
			deviceId = re.findall(r'^\w*\b', readDeviceId[l])[0]
			facility.append(deviceId)

	return facility
