#!/usr/bin/env python
# -*- coding: utf-8 -*-


from selenium import webdriver
from selenium.webdriver.chrome.options import Options

import pyautogui

screenWidth, screenHeight = pyautogui.size() # 屏幕尺寸
mouseX, mouseY = pyautogui.position() # 返回当前鼠标位置，注意坐标系统中左上方是(0, 0)

pyautogui.PAUSE = 1.5 # 每个函数执行后停顿1.5秒
pyautogui.FAILSAFE = True # 鼠标移到左上角会触发FailSafeException，因此快速移动鼠标到左上角也可以停止
