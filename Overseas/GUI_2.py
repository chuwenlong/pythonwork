#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx


class MyFrame(wx.Frame):
	def __init__(self):
		super().__init__(None, title = "Once Word", size = (400, 200), pos = (100, 100))

		# 个人代码
		# 添加静态文本面板
		panel = wx.Panel(parent = self)
		# 将静态文本添加至文本面板
		Itr = wx.StaticText(parent = panel, label = "接口")
		req = wx.StaticText(parent = panel, label = "请求次数")
		interval = wx.StaticText(parent = panel, label = "接口请求间隔")


# 创建应用程序对象
app = wx.App()

# 创建窗口对象
frm = MyFrame()
# 显示窗口
frm.Show()

# 进入主事件循环
app.MainLoop()
