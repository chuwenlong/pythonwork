#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import time

import pandas as pd
import requests as re
from openpyxl import load_workbook


class OverseasAddress:

    def __init__(self):

        self.sr1 = pd.DataFrame(
            columns = ["app_type", "title", "btn_text", "click_url", "icon", "impress_url", "market_url", "notice_url",
                       "source"], index = None)
        self.fileName = 'C:/Users/Administrator/Desktop/海外数据源.xlsx'
        # self.sheetName = '海外数据'

        self.overseas_address = {
            0: "http://ec2-3-249-231-60.eu-west-1.compute.amazonaws.com:8881/getMobPower?app_id=1001&brand=OPPO&google_id=46687ba7-ae5a-40a0-bb34-36df268d325d&lang=br&model=PCLM10&nonce=543867&timestamp=1608617120688&uid=9123678ff2fad1bd&sign=695aeaad714bf7b7e4562b2bf7eff28f",
            1: "http://ec2-3-249-231-60.eu-west-1.compute.amazonaws.com:8881/getOpePosConf?app_id=1001&brand=OPPO&google_id=46687ba7-ae5a-40a0-bb34-36df268d325d&lang=br&model=PCLM10&nonce=543867&timestamp=1608617120688&uid=9123678ff2fad1bd&sign=695aeaad714bf7b7e4562b2bf7eff28f",
            2: "http://ec2-3-249-231-60.eu-west-1.compute.amazonaws.com:8881/getGameApp?app_id=1001&brand=OPPO&google_id=46687ba7-ae5a-40a0-bb34-36df268d325d&lang=br&model=PCLM10&nonce=543867&timestamp=1608617120688&uid=9123678ff2fad1bd&sign=695aeaad714bf7b7e4562b2bf7eff28f"
        }

    def overseas_date(self, num):

        # 创建空文件
        self.sr1.to_excel(self.fileName, index = False, header = True)

        # 获取接口返回数据，插入Excel表格
        length = len(self.overseas_address)
        # print(length)
        try:
            for l in range(length):
                date_str = []
                n = 0
                try:
                    while n < num:
                        try:
                            for data in self.overseas_interface(uri = l):
                                # self.overseas_interface(uri=0)
                                date_str.append({
                                    "app_type": data["app_type"],
                                    "title": data["title"],
                                    "btn_text": data["btn_text"],
                                    "click_url": data["click_url"],
                                    "icon": data["icon"],
                                    "impress_url": data["impress_url"],
                                    "market_url": data["market_url"],
                                    "notice_url": data["notice_url"],
                                    "source": data["source"]
                                })
                            date_str.append({
                                "app_type": "",
                                "title": "",
                                "btn_text": "",
                                "click_url": "",
                                "icon": "",
                                "impress_url": "",
                                "market_url": "",
                                "notice_url": "",
                                "source": ""
                            })

                        except Exception as e:
                            print(f"报错类型{e}")
                        n += 1
                        # print(n)
                except Exception as e:
                    print(f"报错类型{e}")
                self.excel_data(sr3 = date_str, tag_Num = f"接口{l}")
                print(f"接口{l}执行完毕！")
        except Exception as e:
            print(f"报错类型{e}")

        self.del_excel()  # 移除多余的sheet1页
        print("====数据获取成功====")

    # 在新的sheet页添加相应的数据，并保证数据不被覆盖
    def excel_data(self, sr3, tag_Num):

        book = load_workbook(self.fileName)
        writer = pd.ExcelWriter(self.fileName, mode = "a", engine = "openpyxl")
        writer.book = book
        sr1 = pd.DataFrame(sr3, index = None)
        sr1.to_excel(writer, index = False, header = True, sheet_name = f"{tag_Num}")
        writer.save()  # 这步才生成文件
        writer.close()

    # 移除创建表格时多余的sheet页
    def del_excel(self):
        book = load_workbook(self.fileName)
        ws = book["Sheet1"]
        book.remove(ws)
        book.save(self.fileName)

    def overseas_interface(self, uri):

        url = self.overseas_address[uri]
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36 Edg/89.0.774.54"}

        overseas = re.get(url, headers = headers)
        overseas = overseas.json()["msg"]
        js = json.dumps(overseas, sort_keys = True, indent = 4, separators = (',', ': '), ensure_ascii = False)
        # print(js)
        return overseas

    # 获取脚本执行时间
    @property
    def local_times(self):

        times = time.localtime()
        second = times.tm_sec
        minute = times.tm_min
        hour = times.tm_hour

        sum1 = hour * 3600 + minute * 60 + second
        return sum1


if __name__ == "__main__":
    OverseasAddress = OverseasAddress()
    a = OverseasAddress.local_times
    OverseasAddress.overseas_date(2)
    b = OverseasAddress.local_times

    print(f'脚本执行时间：{b - a} S')
