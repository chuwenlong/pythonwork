#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time

import pandas as pd
import requests as re

import api_data
from Overseas.src import to_Excel


class OverseasAddress:

	def __init__(self):

		self.sr1 = pd.DataFrame(
			columns = [
				"app_type",
				"title",
				"btn_text",
				"click_url",
				"icon",
				"impress_url",
				"market_url",
				"notice_url",
				"source"],
			index = None)
		self.fileName = 'C:/Users/Administrator/Desktop/海外数据源.xlsx'
		# self.sheetName = '海外数据'
		api_list = api_data.api_data()
		self.overseas_address = api_list

	def overseas_date(self, num, sec):

		# 创建空文件
		self.sr1.to_excel(self.fileName, index = False, header = True)

		# 获取接口返回数据，插入Excel表格
		length = len(self.overseas_address)

		try:
			n = 0
			while n < num:
				try:
					list1 = []
					# for data in self.overseas_interface(uri = num):
					# 	list1.append(data["title"])

					date_str = []
					for s in range(length):
						for data in self.overseas_interface(uri = f"{s}"):
							data1 = data
							list1.append(data1["title"])
							if list1.count(data1["title"]) > 1 and data1["title"] != "":
								data_value = {
									"app_type": data["app_type"],
									"title": data["title"],
									"btn_text": data["btn_text"],
									"click_url": data["click_url"],
									"icon": data["icon"],
									"impress_url": data["impress_url"],
									"market_url": data["market_url"],
									"notice_url": data["notice_url"],
									"source": data["source"],
									"重复率": "1"
								}
								date_str.append(data_value)

							else:
								data_value = {
									"app_type": data["app_type"],
									"title": data["title"],
									"btn_text": data["btn_text"],
									"click_url": data["click_url"],
									"icon": data["icon"],
									"impress_url": data["impress_url"],
									"market_url": data["market_url"],
									"notice_url": data["notice_url"],
									"source": data["source"]
								}
								date_str.append(data_value)

						self.data_null(date_str)  # 创建空行数据

					to_Excel.excel_data(sr3 = date_str, tag_Num = f"{n + 1}", fileName = self.fileName)

				except Exception as e:
					print(f"报错类型{e}")
				n += 1
				time.sleep(sec)
				print(f"第{n}次接口，执行完毕！")
		except Exception as e:
			print(f"报错类型{e}")

		to_Excel.del_excel(fileName = self.fileName)  # 移除多余的sheet1页
		print("====数据获取成功====")

	# 创建空行
	@staticmethod
	def data_null(str_date):
		str_date.append({
			"app_type": "",
			"title": "",
			"btn_text": "",
			"click_url": "",
			"icon": "",
			"impress_url": "",
			"market_url": "",
			"notice_url": "",
			"source": ""
		})

	# 获取海外接口数据
	def overseas_interface(self, uri):

		url = self.overseas_address[uri]
		headers = {
			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36 Edg/89.0.774.54"}

		overseas = re.get(url, headers = headers)
		overseas = overseas.json()["msg"]
		# js = json.dumps(overseas, sort_keys=True, indent=4, separators=(',', ': '), ensure_ascii=False)
		# # print(js)
		return overseas

	# 获取脚本执行时间
	@property
	def local_times(self):

		times = time.localtime()
		second = times.tm_sec
		minute = times.tm_min
		hour = times.tm_hour

		sum1 = hour * 3600 + minute * 60 + second
		return sum1


if __name__ == "__main__":
	OverseasAddress = OverseasAddress()
	# 获取开始执行的时间
	a = OverseasAddress.local_times
	OverseasAddress.overseas_date(2, 2)
	# 获取执行结算的时间
	b = OverseasAddress.local_times

	print(f'脚本执行时间：{b - a} S')
