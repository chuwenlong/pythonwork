#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx


class MyFrame(wx.Frame):
	def __init__(self):
		super().__init__(None, title = "Once Word", size = (400, 200), pos = (100, 100))

		# 个人代码
		# 添加静态文本面板
		panel = wx.Panel(parent = self)
		# 将静态文本添加至文本面板
		self.staticText = wx.StaticText(parent = panel, label = "请单击OK按钮")

		# 创建按钮
		b = wx.Button(parent = panel, label = "OK")
		# 绑定事件 wx.EVT_BUTTON 是时间类型
		self.Bind(wx.EVT_BUTTON, self.on_click, b)

		# 创建垂直方向的盒子布局管理器对象vbox
		vbox = wx.BoxSizer(wx.VERTICAL)
		# 添加静态文本到vbox布局管理器
		vbox.Add(self.staticText, proportion = 1, flag = wx.ALIGN_CENTER_HORIZONTAL|wx.FIXED_MINSIZE|wx.TOP,
		         border = 30)
		# 添加按钮b到vbox布局管理器
		vbox.Add(b, proportion = 1, flag = wx.EXPAND|wx.BOTTOM, border = 10)
		# 设置面板采用布局管理器
		panel.SetSizer(vbox)

	# 点击事件函数
	def on_click(self, event):
		self.staticText.SetLabelText("Hello World")


# 创建应用程序对象
app = wx.App()

# 创建窗口对象
frm = MyFrame()
# 显示窗口
frm.Show()

# 进入主事件循环
app.MainLoop()
