#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json


def api_data():
	filename = './API_data.json'

	with open(filename) as f:
		api_list = json.load(f)
		api_list = dict(api_list)
	return api_list

# print(api_data())
