#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
from openpyxl import load_workbook


# 在新的sheet页添加相应的数据，并保证数据不被覆盖
def excel_data(sr3, tag_Num, fileName):
    book = load_workbook(fileName)
    writer = pd.ExcelWriter(fileName, mode = "a", engine = "openpyxl")
    writer.book = book
    sr1 = pd.DataFrame(sr3, index = None)
    sr1.to_excel(writer, index = False, header = True, sheet_name = f"{tag_Num}")
    writer.save()  # 这步才生成文件
    writer.close()


# 移除创建表格时多余的sheet页
def del_excel(fileName):
    book = load_workbook(fileName)
    ws = book["Sheet1"]
    book.remove(ws)
    book.save(fileName)
