#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
字符串重新编译：
	若存在重复的字符串，则输出 ')'
	若字符串有且只出现一次，这输出 '('
	且所有 '(' ')' 均需要展示在同一行上
	例如：
		"din"      =>  "((("
		"recede"   =>  "()()()"
		"Success"  =>  ")())())"
		"(( @"     =>  "))(("
"""

# def duplicate_encode(word):
# 	# 创建字符串接收重新编译的 '(' ')'
# 	str1 = ''
# 	# 将所有英文转换为英文小写，因为此题不区分大小写，并使用赋值变量 words 接收，从而达到不对原输入项进行修改
# 	words = word.lower()
# 	# 字符串重新编译
# 	for i in words:
# 		if words.count(i) == 1:
# 			# str1 = str1 + '('
# 			str1 += ''.join("(")
# 		else:
# 			# str1 = str1 + ')'
# 			str1 += ''.join(")")
# 	return str1
#
# print(duplicate_encode("Success"))
# import copy

"""
# 网友的最简洁解决方式
def duplicate_encode(word):
	return "".join(["(" if word.lower().count(c) == 1 else ")" for c in word.lower()])
"""

"""
输入一个整数，超过一个数字，满足属性部分引入在满足如下示例的，输出相同的数字：89
实际上：89 = 8^1 + 9^2
举例：
	sum_dig_pow(1, 10) == [1, 2, 3, 4, 5, 6, 7, 8, 9]
	sum_dig_pow(1, 100) == [1, 2, 3, 4, 5, 6, 7, 8, 9, 89]
"""
# def sum_dig_pow(a, b):
# 	# your code here
# 	listNumber = []
#
# 	for i in range(a, b + 1):
# 		list1 = len(str(i))
# 		list2 = str(i)
# 		if list1 == 1:
# 			listNumber.append(i)
# 		elif list1 > 1:
# 			data = 0
# 			for num in range(list1):
# 				data1 = int(list2[num])**(num + 1)
# 				data += data1
# 			if int(list2) == data:
# 				listNumber.append(data)
#
# 	return listNumber
#
#
# sum_dig_pow(89, 135)
"""
# 网友的最简洁解决方式：
def filter_func(a):
	return sum(int(d) ** (i+1) for i, d in enumerate(str(a))) == a

def sum_dig_pow(a, b):
	return filter(filter_func, range(a, b+1))


# 网友优化解：
def get_digits(n):
	result = []
	while n > 0:
		result.insert(0, n % 10)
		n //= 10
	return result

def sum_dig_pow(a, b):
	result = []
	for n in range(a, b + 1):
		s = 0
		for index, digit in enumerate(get_digits(n)):
			s += digit**(index + 1)
		if s == n:
			result.append(n)
	return result
"""

"""
给定一系列整数，删除最小的值。
注：不要改变原来的阵列/列表。
如果存在具有相同值的多个元素，则删除指数较低的元素。
如果您得到一个空的阵列/列表，返回一个空的阵列/列表。
"""
# list1 = [1, 2, 3, 4, 5]
# def remove_smallest(numbers):
# 	# 复制值给 x ，以确保可以正常移除数据，且原列表不受影响
# 	# remove方法，如果用赋值的方法，会把关联的数据进行改动
# 	x = numbers.copy()
# 	if x:
# 		x.remove(min(x))
# 		return x
# 	else:
# 		return numbers
#
# print(remove_smallest(list1))
"""
网友最优解：
def remove_smallest(numbers):
	a = numbers[:]
	if a:
		a.remove(min(a))
	return a
"""

"""
列表二进制转换
"""
# def binary_array_to_number(arr):
# 	num = 0
# 	list_len = len(arr)
# 	for index, string in enumerate(arr):
# 		if string == 0:
# 			num += 0
# 		elif string == 1:
# 			num += 2 ** ((list_len - 1) - index)
# 	return num
#
#
# a = [1, 0, 1, 1]
# print(binary_array_to_number(a))
"""
网友最优解：
def binary_array_to_number(arr):
	return int("".join(map(str, arr)), 2)
"""

# def binary_array_to_number(arr):
# 	return int("".join(map(str, arr)), 2)
# a = [0, 0, 1, 0]
# print(binary_array_to_number(a))

"""
实现一个功能，它必须采取输入阵列，包含喜欢的项目的人的姓名。
它必须返回示例中显示的显示文本：likes :: [String] -> String

likes([]) # must be "no one likes this"
likes(["Peter"]) # must be "Peter likes this"
likes(["Jacob", "Alex"]) # must be "Jacob and Alex like this"
likes(["Max", "John", "Mark"]) # must be "Max, John and Mark like this"
likes(["Alex", "Jacob", "Mark", "Max"]) # must be "Alex, Jacob and 2 others like this"
"""

# def likes(names):
# 	# your code here
# 	pass
# 	length = len(names)
# 	if length == 0:
# 		return "no one likes this"
# 	elif length == 1:
# 		return f"{names[0]} likes this"
# 	elif length == 2:
# 		return f"{names[0]} and {names[1]} like this"
# 	elif length == 3:
# 		return f"{names[0]}, {names[1]} and {names[2]} like this"
# 	elif length > 3:
# 		return f"{names[0]}, {names[1]} and {length - 2} others like this"
# a = ['Jacob', 'Alex']
# # likes(a)
# print(likes(a))
"""
网友最优解：
def likes(names):
	d = {
		0: "no one likes this",
		1: "{} likes this",
		2: "{} and {} like this",
		3: "{}, {} and {} like this",
		4: "{}, {}, {} and {others} others like this"
	}
	length = len(names)
	return d[min(4, length)].format(*names, others = length - 2)
"""
# def to_jaden_case(sentence):
# 	sentenceUpper = ''
# 	words = sentence.split(" ")
# 	for word in words:
# 		sentenceUpper += word.capitalize() + ' '
#
# 	return sentenceUpper.strip()
#
#
# print(to_jaden_case("How can mirrors be real if our eyes aren't real"))
