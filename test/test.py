#!/usr/bin/env python
# -*- coding: utf-8 -*-

import linecache
import time
import datetime
import xml.sax

# import random
# a = random.randint(0,4)
# def Testgame(self):
#
#     self.b = int(input("请输入你的数字："))
#
#     if a > self.b :
#         print("你输了，要好好努力哦！")
#     elif a == self.b :
#         print("你猜中了人家的小心思！！")
#     else:
#         print("你赢了，你真棒！")
#     print(f"电脑出的数字是：{a}")
#

# A = 1
# for A in range(10):
#     if A < 10:
#         A += 1
#     else:
#         break
#     print(A)


# while A < 10:
#     A += 1
#     print(A)
#
# renwu = random.randint(0,6)
# lad  = {0:'男人',
#         1:'女人',
#         2:'男童',
#         3:'女童',
#         4:'婴儿',
#         5:'老人',
#         6:'情侣'}[renwu]
# print(f'{lad}')


# name = input('请输入你要校验的文件名称：')

# print(list1[1])


# import xml.sax
#
#
# class MovieHandler(xml.sax.ContentHandler):
#         def __init__(self):
#                 self.CurrentData = ""
#                 self.type = ""
#                 self.format = ""
#                 self.year = ""
#                 self.rating = ""
#                 self.stars = ""
#                 self.description = ""
#
#         # 元素开始调用
#         def startElement(self , tag , attributes):
#                 self.CurrentData = tag
#                 if tag == "movie":
#                         print("*****Movie*****")
#                         title = attributes["title"]
#                         print("Title:" , title)
#
#         # 元素结束调用
#         def endElement(self , tag):
#                 if self.CurrentData == "type":
#                         print("Type:" , self.type)
#                 elif self.CurrentData == "format":
#                         print("Format:" , self.format)
#                 elif self.CurrentData == "year":
#                         print("Year:" , self.year)
#                 elif self.CurrentData == "rating":
#                         print("Rating:" , self.rating)
#                 elif self.CurrentData == "stars":
#                         print("Stars:" , self.stars)
#                 elif self.CurrentData == "description":
#                         print("Description:" , self.description)
#                 self.CurrentData = ""
#
#         # 读取字符时调用
#         def characters(self , content):
#                 if self.CurrentData == "type":
#                         self.type = content
#                 elif self.CurrentData == "format":
#                         self.format = content
#                 elif self.CurrentData == "year":
#                         self.year = content
#                 elif self.CurrentData == "rating":
#                         self.rating = content
#                 elif self.CurrentData == "stars":
#                         self.stars = content
#                 elif self.CurrentData == "description":
#                         self.description = content
#
#
# if (__name__ == "__main__"):
#         # 创建一个 XMLReader
#         parser = xml.sax.make_parser()
#         # 关闭命名空间
#         parser.setFeature(xml.sax.handler.feature_namespaces , 0)
#
#         # 重写 ContextHandler
#         Handler = MovieHandler()
#         parser.setContentHandler(Handler)
#
#         parser.parse("movies.xml")


# 获取当前时间
# dtime = datetime.datetime.now()
# un_time = time.mktime(dtime.timetuple())
# print(un_time)
# # 将unix时间戳转换为“当前时间”格式
# times = datetime.datetime.fromtimestamp(un_time)
# print(times)
#
# timeStamp = 1605943525.290
# timeArray = time.localtime(timeStamp)
# otherStyleTime = time.strftime(f"%Y-%m-%d %H:%M:%S", timeArray)
# print(otherStyleTime)


# import json
# from pygal_maps_world.i18n import COUNTRIES
# import pygal
# from pygal.style import LightColorizedStyle as LCS,RotateStyle as RS
#
#
# def get_country_code(country_name):
# 	"""定义一个获取国别码的函数 """
# 	for code,name in COUNTRIES.items():
# 		# 只获取指定国家的国别码
# 		if name == country_name:
# 			return code
# 	return None
#
#
# # 读取世界人数数据集中的人口数据
# filename = "population_data.json"
# with open(filename) as f:
# 	pop_data = json.load(f)
# # 创建一个存储国别码：人口的列表
# dir_population = {}
# for pop_dict in pop_data:
# 	# 读取其中2010年人口数据
# 	if pop_dict["Year"] == "2017":
# 		country_name = pop_dict["Country Name"]
# 		population = int(float(pop_dict["Value"]))
# 		code = get_country_code(country_name)
# 		if code:
# 			dir_population[code] = population
# # 绘制世界人口地图
# wm_style = RS('#990000',base_style = LCS)  # 加亮颜色主题
# wm = pygal.maps.world.World(style = wm_style)
# wm.title = "Global Population in 2010 by Country"
# wm.add("2010",dir_population)
# wm.render_to_file("Global_Population.svg")
# print(123)

import time

locatime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
t = int(time.strftime("%Y", time.localtime()))
date = time.localtime()
year,month,day=date[:3]
day_month=[31,28,31,30,31,30,31,31,30,31,30,31]

if t%100 == 0 and t%400 == 0:
    print('今年是闰年')
    day_month[1]=29
    if month == 1:
        rest_time = 366 - day
        print(f'今年剩余：{rest_time}天')
    else:
        rest_time = 366 - (sum(day_month[:month-1])+day)
        print(f'今年剩余：{rest_time}天')
elif t%100 != 0 and t%4 == 0:
    print('今年是闰年')
    day_month[1]=29
    if month == 1:
        rest_time = 366 - day
        print(f'今年剩余：{rest_time}天')
    else:
        rest_time = 366 - (sum(day_month[:month-1])+day)
        print(f'今年剩余：{rest_time}天')
else:
    print('今年是平年')
    if month == 1:
        rest_time = 365 - day
        print(f'今年剩余：{rest_time}天')
    else:
        rest_time = 365 - (sum(day_month[:month-1])+day)
        print(f'今年剩余：{rest_time}天')

import turtle, time
import winsound
import math

locatime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

def drawGap():
    turtle.penup()
    turtle.fd(5)

def drawLine(draw):
    drawGap()
    turtle.pendown() if draw else turtle.penup()
    turtle.fd(40)
    drawGap()
    turtle.right(90)

def drawDigit(d):
    drawLine(True) if d in [2, 3, 4, 5, 6, 8, 9] else drawLine(False)  # g
    drawLine(True) if d in [0, 1, 3, 4, 5, 6, 7, 8, 9] else drawLine(False)  # c
    drawLine(True) if d in [0, 2, 3, 5, 6, 8, 9] else drawLine(False)  # d
    drawLine(True) if d in [0, 2, 6, 8] else drawLine(False)  # e
    turtle.left(90)  # 经历一次右转后，调整左转，方向竖直向上
    drawLine(True) if d in [0, 4, 5, 6, 8, 9] else drawLine(False)
    drawLine(True) if d in [0, 2, 3, 5, 6, 7, 8, 9] else drawLine(False)
    drawLine(True) if d in [0, 1, 2, 3, 4, 7, 8, 9] else drawLine(False)
    turtle.left(180)
    turtle.penup()
    turtle.fd(20)

def drawDate(date):
    turtle.pencolor('red')
    if '+' in date:
        for i in date:
            if i == '-':
                turtle.write('时', font=('Arial', 18, 'normal'))
                turtle.pencolor('green')
                turtle.fd(40)
            elif i == '=':
                turtle.write('分', font=('Arial', 18, 'normal'))
                turtle.pencolor('blue')
                turtle.fd(40)
            elif i == '+':
                turtle.write('秒', font=('Arial', 18, 'normal'))
                turtle.pencolor('yellow')
                turtle.fd(40)
            else:
                drawDigit(eval(i))
    else:
        turtle.goto(-180,-50)
        turtle.write(date, font=('Arial', 70, 'normal'))   # 打印超时文字

def init():
    turtle.setup(540, 540, 0, 0)  # 设置画布大小 200 200 为屏幕位置
    turtle.speed(10)
    turtle.penup()
    turtle.goto(0, 0)
    turtle.fd(-200)     # 画笔起始位置
    turtle.pensize(5)

def main(book_time):
    over_flag = False
    beep_flag = True
    count = book_time - time.time() + 0    # 画面延时n秒自动关闭
    try:
        while True:
            over_time = book_time - time.time()
            count -= 1
            if over_time > 0:
                time_string = time.strftime("%M=%S+", time.localtime(over_time))
            else:
                time_string = '下班了！！'
                over_flag = True
            turtle.clear()
            init()
            turtle.getscreen().tracer(30, 0)
            drawDate(time_string)  # 格式化时间 2017-05=02+ 控制输入年日月
            time.sleep(1)
            turtle.hideturtle()
            if over_flag and beep_flag:
                for i in range(100):
                    winsound.Beep(int(600 * math.sin(i / 6.28) + 700), 100)
                beep_flag = False
    except:
        pass

minites = 60 * 0.1
book_time = time.time() + minites
main(book_time)

