#!/usr/bin/env python
# -*- coding: utf-8 -*-

import turtle, time
import winsound
import math

locatime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

def drawGap():
    turtle.penup()
    turtle.fd(5)

def drawLine(draw):
    drawGap()
    turtle.pendown() if draw else turtle.penup()
    turtle.fd(40)
    drawGap()
    turtle.right(90)

def drawDigit(d):
    drawLine(True) if d in [2, 3, 4, 5, 6, 8, 9] else drawLine(False)  # g
    drawLine(True) if d in [0, 1, 3, 4, 5, 6, 7, 8, 9] else drawLine(False)  # c
    drawLine(True) if d in [0, 2, 3, 5, 6, 8, 9] else drawLine(False)  # d
    drawLine(True) if d in [0, 2, 6, 8] else drawLine(False)  # e
    turtle.left(90)  # 经历一次右转后，调整左转，方向竖直向上
    drawLine(True) if d in [0, 4, 5, 6, 8, 9] else drawLine(False)
    drawLine(True) if d in [0, 2, 3, 5, 6, 7, 8, 9] else drawLine(False)
    drawLine(True) if d in [0, 1, 2, 3, 4, 7, 8, 9] else drawLine(False)
    turtle.left(180)
    turtle.penup()
    turtle.fd(20)

def drawDate(date):
    turtle.pencolor('red')
    if '+' in date:
        for i in date:
            if i == '-':
                turtle.write('时', font=('Arial', 18, 'normal'))
                turtle.pencolor('green')
                turtle.fd(40)
            elif i == '=':
                turtle.write('分', font=('Arial', 18, 'normal'))
                turtle.pencolor('blue')
                turtle.fd(40)
            elif i == '+':
                turtle.write('秒', font=('Arial', 18, 'normal'))
                turtle.pencolor('yellow')
                turtle.fd(40)
            else:
                drawDigit(eval(i))
    else:
        turtle.goto(-180,-50)
        turtle.write(date, font=('Arial', 70, 'normal'))   # 打印超时文字

def init():
    turtle.setup(540, 540, 0, 0)  # 设置画布大小 200 200 为屏幕位置
    turtle.speed(10)
    turtle.penup()
    turtle.goto(0, 0)
    turtle.fd(-200)     # 画笔起始位置
    turtle.pensize(5)

def main(book_time):
    over_flag = False
    beep_flag = True
    count = book_time - time.time() + 0    # 画面延时n秒自动关闭
    try:
        while True:
            over_time = book_time - time.time()
            count -= 1
            if over_time > 0:
                time_string = time.strftime("%M=%S+", time.localtime(over_time))
            else:
                time_string = '下班了！！'
                over_flag = True
            turtle.clear()
            init()
            turtle.getscreen().tracer(30, 0)
            drawDate(time_string)  # 格式化时间 2017-05=02+ 控制输入年日月
            time.sleep(1)
            turtle.hideturtle()
            if over_flag and beep_flag:
                for i in range(100):
                    winsound.Beep(int(600 * math.sin(i / 6.28) + 700), 100)
                beep_flag = False
    except:
        pass

minites = 60 * 0.1
book_time = time.time() + minites
main(book_time)
