#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import zipfile

# file_name = 'E:\\动效包校验\manifest.xml'
list1 = ['try', 'bottom_click']
file1 = 'E:\\动效包校验'

'''
root  当前目录路径 
dirs  当前路径下所有子目录 
files 当前路径下所有非目录子文件
'''


# 获取文件夹内的压缩包
def file_name1(file_dir = file1):
	# list_name = []
	# list_extension = []

	for root, dirs, files in os.walk(file_dir):
		# print(root)  # 当前目录路径
		# print(dirs)  # 当前路径下所有子目录
		print(files)  # 当前路径下所有非目录子文件

		for file in files:
			if os.path.splitext(file)[1] == '.zip':
				# list_name.append(os.path.join(f'{root}\\{file}'))
				extension = os.path.splitext(file)[0]
				# list_extension.append(os.path.join(root, extension))
				list_name = os.path.join(f'{root}\\{file}')
				list_extension = os.path.join(f'{root}\\{extension}')
				file_name3 = f'E:\\动效包校验\\{extension}\manifest.xml'

	# A =

	tuple1 = (list_name, list_extension, file_name3)
	# print(tuple1)
	return tuple1


zip1 = file_name1()[0]
dst1 = file_name1()[1]
file_name = file_name1()[2]


# 解压zip
def unzip_file(zip_src = zip1, dst_dir = dst1):
	r = zipfile.is_zipfile(zip_src)
	if r:
		fz = zipfile.ZipFile(zip_src, 'r')
		for file in fz.namelist():
			fz.extract(file, dst_dir)
		print('==解压成功==')
	else:
		print('This is not zip')


# 判断动效包
def get_dongxiao():
	with open(file_name, 'r', encoding = 'utf-8') as file_obj:
		for content in file_obj:
			content1 = content.rstrip()
			# print(content1)
			if list1[0] in content1:
				A = '这是投屏试玩的动效包'
				print(A)
			elif list1[1] in content1:
				B = '这是本地玩的动效包'
				print(B)
		# elif list1 not in content1:
		# 	print('这不是游戏动效包')


# 压缩zip
def zip_file(src_dir = dst1):

	zip_name = src_dir + '-' + input() + '.zip'
	z = zipfile.ZipFile(zip_name, 'w', zipfile.ZIP_DEFLATED)
	for dirpath, dirnames, filenames in os.walk(src_dir):
		fpath = dirpath.replace(src_dir, '')
		fpath = fpath and fpath + os.sep or ''
		for filename in filenames:
			z.write(os.path.join(dirpath, filename), fpath + filename)
		print('==压缩成功==')
	z.close()


if __name__ == "__main__":
	file_name1()
	unzip_file()
	get_dongxiao()
	zip_file()
