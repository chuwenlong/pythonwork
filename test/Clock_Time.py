#!/usr/bin/env python
# -*- coding: utf-8 -*-


import tkinter as tk
import time


# '''秒表部件'''
# class StopWatch:
#     def __init__(self,se):
#         self.se=se
#         self.run=False
#     def update(self):
#         self.se=time.time()-self.start
#         self.setTime(self.se)
#         self.timer=root.after(50,self.update)
#     def setTime(self,tm):
#         hours=int(tm/60/60)
#         minutes=int(tm/60)
#         seconds=int(tm-minutes*60.0)
#         hseconds=int((tm-minutes*60.0-seconds)*100)
#         self.var.set('%.2d:%.2d:%.2d:%.2d'%(hours,minutes,seconds,hseconds))
#     def Start(self):
#         if not self.run:
#             self.start=time.time()-self.se
#             print(self.start)
#             self.update()
#             self.run=True
#     def Stop(self):
#         if self.run:
#             root.after_cancel(self.timer)
#             self.se=time.time()-self.start
#             self.setTime(self.se)
#             self.run=False
#     def Reset(self):
#         if not self.run:
#             self.se=0
#             self.setTime(self.se)
#     def stopwatch(self):
#         win=tk.Toplevel()
#         win.geometry('192x100')
#         win.title('秒表')
#         self.var=tk.StringVar()
#         l=tk.Label(win,textvariable=self.var,font=('',35))
#         l.pack(expand=1)
#         self.setTime(self.se)
#         panel2=tk.Frame(win)
#         panel2.pack(expand=1)
#         tk.Button(panel2,text='开始计时',command=self.Start).pack(side='left')
#         tk.Button(panel2,text='停止计时',command=self.Stop).pack(side='left')
#         tk.Button(panel2,text='复位',command=self.Reset).pack(side='left')
#         st.config(text='计次')
# '''秒表部件'''

'''时钟部件'''
def settime():
    # 星期
    wd = int(time.strftime('%w'))
    b = {1: "星期一",
         2: '星期二',
         3: '星期三',
         4: '星期四',
         5: '星期五',
         6: '星期六',
         0: '星期日'}
    d = b[wd]

# 日期和时间
    today1=time.strftime('%Y年%m月%d日')+d
    h=int(time.strftime('%H'))                          # 获取小时的时间
    m=int(time.strftime('%M'))                          # 获取分钟的时间
    s=int(time.strftime('%S'))                          # 获取秒的时间
    time1=f'今日剩余时间：\n{24-h}小时{60-m}分钟{60-s}秒'          # 展示今天剩余时间
    time2=time.strftime('%H:%M:%S')                     # 展示本地时间


    h=int(time.strftime('%H'))                          # 获取小时的时间
    m=int(time.strftime('%M'))                          # 获取分钟的时间
    s=int(time.strftime('%S'))                          # 获取秒的时间

    # locatime=time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())
    t=int(time.strftime("%Y",time.localtime()))
    date=time.localtime()
    year,month,day=date[:3]
    day_month=[31,28,31,30,31,30,31,31,30,31,30,31]

    if t%100==0 and t%400==0 or (t%100!=0 and t%4==0):
        # print('今年是闰年')
        years = '闰年'
        day_month[1]=29
        if month==1:
            rest_time = f'今年剩余：{366-day}'
            year_1=rest_time*24-h
            year_2=year_1*60-m
            year_3=year_2*60-s
            # print(rest_time)
        else:
            rest_time = 366-(sum(day_month[:month-1])+day)
            year_1 = rest_time*24-h
            year_2 = year_1*60-m
            year_3 = year_2*60-s
            # print(rest_time)
    else:
        # print('今年是平年')
        years='平年'
        if month == 1:
            rest_time = f'今年剩余：{365-day}'
            year_1=rest_time*24-h
            year_2=year_1*60-m
            year_3=year_2*60-s
            # print(rest_time)
        else:
            rest_time = f'今年剩余：{365-(sum(day_month[:month-1])+day)}'
            year_1=rest_time*24-h
            year_2=year_1*60-m
            year_3=year_2*60-s
            # print(rest_time)
    time4 = f'今年是{years} \n 剩余：\n {rest_time}天 \n {year_1}小时 \n {year_2}分钟 \n{year_3}秒'


    var1.set(today1)
    var2.set(time1)
    var3.set(time2)
    var4.set(time4)
    root.after(1000,settime)
'''时钟部件'''

locatime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
t = int(time.strftime("%Y", time.localtime()))
date = time.localtime()
year,month,day=date[:3]
day_month=[31,28,31,30,31,30,31,31,30,31,30,31]




'''程序入口'''
root=tk.Tk()
root.title('时钟')
var1=tk.StringVar()
var2=tk.StringVar()
var3=tk.StringVar()
var4=tk.StringVar()

# 确认时间的字体和大小
l2=tk.Label(root,textvariable=var2,font=('宋体',20))
l1=tk.Label(root,textvariable=var1,font=('宋体',25))
l3=tk.Label(root,textvariable=var3,font=('宋体',20))
l4=tk.Label(root,textvariable=var4,font=('宋体',20))

l1.pack(expand=1)
l3.pack(expand=1)
l2.pack(expand=1)
l4.pack(expand=1)

settime()
panel1=tk.Frame(root)
panel1.pack(expand=1)
# sw=StopWatch(0)
# st=tk.Button(panel1,text='倒计时',command=sw.stopwatch)
# st.pack()
root.mainloop()
'''程序入口'''


