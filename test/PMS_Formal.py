#!/usr/bin/env python
# -*- coding: utf-8 -*-

from selenium import webdriver
import unittest
from time import sleep
from selenium.webdriver.common.keys import Keys


class Pms_Platform(unittest.TestCase):


    def setUp(self) -> None:

        '''
        打开火狐浏览器
        输入广告平台网址
        窗口最大化
        '''

        self.driver = webdriver.Firefox()
        self.driver = webdriver.Firefox()
        self.driver.get('http://adx.zookingsoft.com/dsp/#/Welcome/Login')
        self.driver.maximize_window()
        sleep(5)

    def test_login(self):

        # 输入用户名
        self.driver.find_element_by_name('username').send_keys('longchuwen@zookingsoft.com')
        sleep(2)    # 睡眠2S
        # 输入密码
        self.driver.find_element_by_name('password').send_keys('644265426+lc')
        sleep(2)    # 睡眠2S
        # 点击登录按钮
        self.driver.find_element_by_id('login_submit_btn').click()
        sleep(2)

        self.driver.find_element_by_xpath('/html/body/div[1]/article/div/div/div[1]/a[2]').click()
        sleep(3)
        self.driver.find_element_by_xpath('/html/body/div[1]/article/div/div/div[1]/a[3]').click()
        sleep(3)

        self.driver.find_element_by_xpath('//*[@id="put_date"]').click()
        sleep(3)

        self.driver.find_element_by_xpath('/html/body/div[13]/div[2]/div[1]/input').click()
        sleep(2)
        # self.driver.find_element_by_xpath('/html/body/div[13]/div[2]/div[1]/input').send_keys(Keys.CONTROL,'a')
        # sleep(2)
        # self.driver.find_element_by_xpath('/html/body/div[13]/div[2]/div[1]/input').send_keys(Keys.BACK_SPACE)
        self.driver.find_element_by_xpath('/html/body/div[13]/div[2]/div[1]/input').clear()
        sleep(3)


    # def tearDown(self) -> None:
    #     self.driver.quit()


if __name__ == "__main__":
    unittest.main()
