#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import zipfile


# 获取当前文件夹下的文件
# path = "../"
# fileList = os.listdir(path)
# print(fileList)

# 获取app名称
def appName(name, path):
	fileList = os.listdir(path)
	for item in fileList:
		if name in item:
			item = item.split(".")
			return item[0]
	return "未找到文件"


# 获取当前文件夹路径
def filePath():
	path_file = __file__
	path_file = path_file.split("\\")
	path_file.pop(-1)
	path_file.pop(-1)
	path_file = "\\".join(path_file)
	return path_file


# 获取当前路径下对应文件
def implement(path_route, data=None, num=0):
	if num == 0:
		data = [filePath(), path_route]
		data_implement = "\\".join(data)
		return data_implement
	elif num == 1:
		data = [filePath(), data, path_route]
		data_implement = "\\".join(data)
		return data_implement


# 解压压缩包
def zip_file():
	filename = f'../{appName("本地玩", "../")}.zip'
	file_zip = zipfile.ZipFile(filename)
	try:
		# 类似tar解除打包，建立文件夹存放解压的多个文件
		if not os.path.isdir(filename):
			os.mkdir(filename + "_dir")
		for names in file_zip.namelist():
			file_zip.extract(names, filename + "_dir/")
		file_zip.close()
	except Exception as e:
		print(f"错误类型：{e}")


