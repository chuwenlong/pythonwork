#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

import obtain_app_name as app
from file_upload import file_up
from obtain_app_name import appName


# 设置智能等待方法
def element(driver, route, name):
	elements = WebDriverWait(driver, 10, 0.5).until(EC.visibility_of_element_located((route, name)))
	return elements


# 重命名文件夹名称
def up_image():
	time.sleep(3)
	data1 = appName("_dir", "../")
	implement = f'{app.implement(f"{data1}.zip_dir")}'
	data3 = app.implement(appName(".png", implement), data = f'{data1}.zip_dir', num = 1)
	file_up("打开", data3, f".png")
	time.sleep(5)


# 设置文件展示的等待方法
def waiting(driver, route, name, value):
	text = ""
	while text == "":
		text = driver.find_element(route, name).get_attribute(value)
		time.sleep(2)


class Application:

	def __init__(self):
		# 打开浏览器
		self.browser = webdriver.Chrome()
		# 窗口最大化
		self.browser.maximize_window()
		# 等待3s
		time.sleep(3)
		# 输入广告平台DSP网址
		self.browser.get("http://adx.zookingsoft.com/dsp/#/Content/Home/View")
		element(self.browser, By.NAME, "username")

		# 登录网站
		self.browser.find_element(By.NAME, "username").send_keys("longchuwen@zookingsoft.com")
		self.browser.find_element(By.NAME, "password").send_keys("644265426+lc")
		self.browser.find_element(By.ID, "login_submit_btn").click()

	# 上传应用
	def app_upload(self):
		element(self.browser, By.ID, "tool")
		self.browser.find_element(By.ID, "tool").click()
		# 点击【新建应用】
		element(self.browser, By.XPATH, '//*[@id="search_app_form"]/div/div[3]/span')
		self.browser.find_element(By.XPATH, '//*[@id="search_app_form"]/div/div[3]/span').click()

		# 输入应用信息
		element(self.browser, By.NAME, 'appName')
		self.browser.find_element(By.NAME, 'appName').send_keys(appName(".apk", "../"))
		# 上传应用
		self.browser.find_element(By.ID, "apk_upload_btn").click()
		time.sleep(3)
		file_up("打开", app.implement(appName(".apk", "../")), ".apk")

		# 等待应用上传成功
		Names = self.browser.find_element(By.XPATH, '//*[@id="create_app"]/form/div[10]/span').text
		appNames = self.browser.find_element(By.XPATH, '//*[@id="create_app"]/form/div[10]/span').text
		while appNames == Names:
			appNames = self.browser.find_element(By.XPATH, '//*[@id="create_app"]/form/div[10]/span').text

		# 等待3s
		time.sleep(3)

		try:
			# 解压本地玩包
			fileList1 = os.listdir("../")
			if f'{appName("_dir", "../")}' not in fileList1:
				app.zip_file()
		except Exception as e:
			print(f"错误类型：{e}")

		# 上传ICON
		self.browser.find_element(By.ID, "apk_icon_btn").click()
		up_image()

		# 等待3s
		time.sleep(3)
		# 上传应用截图
		self.browser.find_element(By.ID, "apk_screen_shot_btn").click()
		up_image()

		# 输入文本数据
		self.browser.find_element(By.NAME, "h5Introduce").send_keys("测试H5介绍页")
		self.browser.find_element(By.XPATH, '//*[@id="apk_desc"]/textarea').send_keys("测试应用描述")
		# 点击提交按钮
		# browser.find_element(By.XPATH, '//*[@id="create_app"]/form/div[17]/div[2]').click()
		# 点击取消按钮
		self.browser.find_element(By.XPATH, '//*[@id="create_app"]/form/div[17]/div[1]').click()

	# 删除apk
	# os.remove(app.implement(appName()) + ".apk")

	# 应用上传操作
	def app_up(self):
		try:
			# 点击【工具】
			self.app_upload()

			print("\n应用上传完成！")
			time.sleep(2)

		except Exception as e:
			print(f"错误类型：{e}")

	def local_game(self):
		try:
			# 登录
			# self.web_login()
			# 等待
			element(self.browser, By.ID, "adplay")
			# 点击【试玩管理】
			self.browser.find_element(By.ID, 'adplay').click()
			time.sleep(3)

			# 点击【本地玩包管理】
			self.browser.find_element(By.XPATH, '//*[@id="ad_tabs"]/a[5]').click()
			element(self.browser, By.XPATH, '//*[@id="channel_mgr_search"]/div/div[2]/span')

			# 点击【新建本地玩包】
			self.browser.find_element(By.XPATH, '//*[@id="channel_mgr_search"]/div/div[2]/span').click()

			# 输入本地玩信息
			element(self.browser, By.NAME, "splash")
			self.browser.find_element(By.NAME, "splash").send_keys(appName(".apk", "../"))
			self.browser.find_element(By.ID, "apk_upload_btn").click()
			time.sleep(3)
			file_up("打开", app.implement(appName("本地玩", "../")), ".zip")

			# 获取并判断 input 标签的 value 值
			waiting(self.browser, By.XPATH, '//*[@id="create_local_play"]/form/div[5]/div/input', "value")
			# 点击【确定】按钮
			# self.browser.find_element(By.CSS_SELECTOR, '#create_local_play > form > div:nth-child(10) > div > div.btn.btn-success').click()
			# 点击【取消】按钮
			self.browser.find_element(By.CSS_SELECTOR, '#create_local_play > form > div:nth-child(10) > div > div.btn.btn-default').click()

			print("\n本地玩上传完成！")

		except Exception as e:
			print(f"错误类型：{e}")


if __name__ == "__main__":

	while True:
		input_data = input("""1. 仅上传应用
2. 仅上传本地玩包
3. 既需要上传应用，同时也要上传本地玩包
请选择你要实现的功能序号：""")
		if input_data == '1':
			Application = Application()
			Application.app_up()
			break
		elif input_data == '2':
			Application = Application()
			Application.local_game()
			break
		elif input_data == '3':
			Application = Application()
			Application.app_up()
			Application.local_game()
			break
		else:
			print("请输入正确的序号！")
