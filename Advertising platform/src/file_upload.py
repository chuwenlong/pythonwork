#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time

import win32con
import win32gui


# 操作windows上传窗口
def file_up(name, path, apk):
	# 调用win32库上传文件
	dialog = win32gui.FindWindow('#32770', f'{name}')  # 对话框    name: 上传文件窗口的名称
	ComboBoxEx32 = win32gui.FindWindowEx(dialog, 0, 'ComboBoxEx32', None)
	ComboBox = win32gui.FindWindowEx(ComboBoxEx32, 0, 'ComboBox', None)
	Edit = win32gui.FindWindowEx(ComboBox, 0, 'Edit', None)
	# 上面三句依次寻找对象，直到找到输入框Edit对象的句柄
	button = win32gui.FindWindowEx(dialog, 0, 'Button', None)
	# 确定按钮Button
	# 自动识别window弹窗，输入绝对路径上传图片
	time.sleep(3)
	fileDir = f"{path}{apk}"
	win32gui.SendMessage(Edit, win32con.WM_SETTEXT, None, fileDir)  # 往输入框输入绝对地址
	time.sleep(2)
	win32gui.SendMessage(dialog, win32con.WM_COMMAND, 1, button)  # 按button
