#!/usr/bin/env python
# -*- coding: utf-8 -*-

from openpyxl import load_workbook

fileName = 'D:/TestWork/Automation/BaiDu_Automation_Test.xlsx'

# 以只读模式打开工作簿
wb = load_workbook(filename = fileName, read_only = True)

sheetName = "Sheet1"
sheet = wb.create_sheet('test')
ws = wb[sheetName]
wb.save(fileName)
print(wb)
