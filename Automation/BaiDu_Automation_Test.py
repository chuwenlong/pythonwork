#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time

from selenium import webdriver
from selenium.webdriver.common.by import By

url = "https://www.baidu.com/"
attribute = By.ID
content = "kw"
key = "热搜榜"


# 初始化浏览器
def test_baidu_automation(uri, atb, con, keys):
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get(uri)

    driver.find_element(atb, con).send_keys(keys)
    time.sleep(2)

    driver.find_element(atb, con).click()
    time.sleep(2)

    driver.quit()
    print('测试完成')


if __name__ == '__main__':
    test_baidu_automation(url, attribute, content, key)
