#!/usr/bin/env python
# -*- coding: utf-8 -*-

# times = time.localtime()
# year = times.tm_year
# mon = times.tm_mon
# day = times.tm_mday
# sum1 = f'{year}-{mon}-{day}'
# s = ["Alex", "Jacob", "Mark", "Max"]
# d = {
#         0: "no one likes this",
#         1: "{} likes this",
#         2: "{} and {} like this",
#         3: "{}, {} and {} like this",
#         4: "{}, {}, {} and {others} others like this"
# }
# length = len(s)
# a = d[min(4, length)].format(*s, others = length - 2)
# print(a)

# a = "Most Most Trees Are Blue"
# str1 = a.split(" ")
# str2 = str1[0].title()
# str3 = list(set(str1))
# str3.sort(key = str1.index)
# print(str3)


# path = __file__
# path = path.split("/")
# path.pop(-1)
# path = "/".join(path)
# print(path)
