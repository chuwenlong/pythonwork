#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time

# import json
import pymysql
import requests as re


def weather_requests():
	uri = "http://apis.juhe.cn/simpleWeather/query"

	param = {
		"city": "深圳",
		"key": "802e34428a9e3eed1847c5273653e861"
	}

	headers = {
		"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 Edg/90.0.818.56"
	}

	weather_data = re.request("get", uri, params = param, headers = headers)
	return weather_data


def request_data():
	weather_data = weather_requests()
	# 获取相关天气参数
	weather_data = weather_data.json()
	data = weather_data["result"]["realtime"]
	city = weather_data["result"]["city"]
	future = weather_data["result"]["future"]
	# js = json.dumps(weather_data, indent = 4, separators = (',', ':'), ensure_ascii = False)
	return data, city, future


def api_database(data, city, future):
	# 打开数据库连接
	db = pymysql.connect(
		host = "localhost",
		port = 3306,
		user = "root",
		password = "",
		db = "weather_api",
		charset = "utf8"
	)

	# 获取执行时间
	times = time.localtime()
	year = times.tm_year
	mon = times.tm_mon
	day = times.tm_mday
	hour = times.tm_hour
	minute = times.tm_min
	sec = times.tm_sec
	sum1 = f'{year}-{mon}-{day} {hour}:{minute}:{sec}'

	values_list1 = [city]
	# 使用cursor()方法创建一个游标对象
	cursor = db.cursor()

	# 将当天查询的未来天气情况数据录入数据库
	for i in future:
		future_list1 = [city, str(sum1)]
		for key1, values1 in enumerate(i):
			if values1 == "wid":
				continue
			future_list1.append(i[values1])
		cursor.execute(
			"insert into weather_future(city, now_time, future_date, temperature, weather, direct) values(%s, %s, %s, %s, %s, %s)",
			future_list1)
	# print(future_list1)
	values_list1.append(str(sum1))

	# 将当天查询的当天天气数据录入数据库
	for key1, values in enumerate(data):
		values_list1.append(data[values])
	# print(values_list1)
	cursor.execute(
		"insert into weather_now(city, now_time, temperature, humidity, info, wid, direct, power, aqi) values(%s, %s, %s, %s, %s, %s, %s, %s, %s)",
		values_list1)

	cursor.execute("SELECT * from weather_now;")
	# 使用fetall()获取全部数据
	data1 = cursor.fetchall()
	# 打印获取到的数据
	# print(data1)

	# 关闭游标和数据库的连接
	db.commit()
	cursor.close()
	db.close()


if __name__ == "__main__":
	data2, city1, future1 = request_data()
	api_database(data2, city1, future1)
	print("数据请求完成，并成功进入数据库")
